<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerSxuxIxz\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerSxuxIxz/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerSxuxIxz.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerSxuxIxz\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerSxuxIxz\App_KernelDevDebugContainer([
    'container.build_hash' => 'SxuxIxz',
    'container.build_id' => '384171be',
    'container.build_time' => 1655290080,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerSxuxIxz');
