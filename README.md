## Project rest api based on that descrption

You have the following database: 
Product (Id, name, short description, long description, main image, created date, updated date, price, category)
Category (Id, name)
Image (id, name, url, short description, created date, updated date)

Can you develop a REST API using Symfony to fetch Product data for a listing to show the product name, category and the price.


## Routing

 ---------------- -------- -------- ------ -------------------------- 
  Name             Method   Scheme   Host   Path                      
 ---------------- -------- -------- ------ -------------------------- 
  _preview_error   ANY      ANY      ANY    /_error/{code}.{_format}  
  product_index    GET      ANY      ANY    /product                  
  product_new      POST     ANY      ANY    /product                  
  product_show     GET      ANY      ANY    /product/{id}             
  product_edit     PUT      ANY      ANY    /product/{id}             
  product_delete   DELETE   ANY      ANY    /product/{id}             
 ---------------- -------- -------- ------ -------------------------- 


## The Project

 - to start use = symfony server:start
 - to install at the first time use = composer install
 - to create a migration = php bin/console make:migration
 - to migrate = php bin/console doctrine:migrations:migrate

## Objections 

- I made was asked to me and If I have much more time, there are so many things that are needed to change here and implement jwt to protect the system.

- Then are another thing that are needed here: 
- Implement mabe one handler.

- Lastly we need to see right requiments to finish this.

- Thanks again, this was made by Salomão and in 3.5 hours ok


